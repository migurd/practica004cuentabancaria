
package Controlador;
// Se importa el modelo y vistas
import Modelo.Cliente;
import Modelo.CuentaBancaria;
import Vista.dlgCuentaBancaria;
// Se importan los actionListeners
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
// Se importan las cosas de vista
import javax.swing.JFrame;
import javax.swing.JOptionPane;


/**
 *
 * @author quier
 */
public class Controlador implements ActionListener {
    private CuentaBancaria cuenta;
    private dlgCuentaBancaria vista;
    
    public Controlador(CuentaBancaria cuenta, dlgCuentaBancaria vista) {
        this.cuenta = cuenta;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnDeposito.addActionListener(this);
        vista.btnRetiro.addActionListener(this);
    }
    
    public static void main(String[] args) {
        //System.out.println("ola");
        CuentaBancaria cuenta = new CuentaBancaria();
        dlgCuentaBancaria vista = new dlgCuentaBancaria(new JFrame(), true);
        Controlador contra = new Controlador(cuenta, vista);
        contra.iniciarVista();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnNuevo) {
            limpiar();
            vista.txtNumCuenta.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.txtFechaNacimiento.setEnabled(true);
            vista.txtNombreBanco.setEnabled(true);
            vista.txtFechaApertura.setEnabled(true);
            vista.txtPorcentajeRendimiento.setEnabled(true);
            vista.txtSaldo.setEnabled(true);
            vista.radioButtonSexoMasculino.setEnabled(true);
            vista.radioButtonSexoFemenino.setEnabled(true);
            
            // Unlocks save
            vista.btnGuardar.setEnabled(true);
            
            // Locks other options
            
        }
        
        if(e.getSource() == vista.btnGuardar) {
            String option = "";
            try {
                if(vista.radioButtonSexoMasculino.isSelected() == true)
                    option = "Sexo Masculino";
                else if(vista.radioButtonSexoFemenino.isSelected() == true)
                    option = "Sexo Femenino";
                else
                    throw new IllegalArgumentException("No elegiste un sexo...");
                
                // Making illegal negatives, and 0 on num cuenta
                if(Float.parseFloat(vista.txtNumCuenta.getText()) <= 0)
                    throw new IllegalArgumentException("No se aceptan negativos ni nulos en el número de cuenta...");
                else if(Float.parseFloat(vista.txtSaldo.getText()) < 0)
                    throw new IllegalArgumentException("No se aceptan negativos en saldo...");
                
                // For some reason it doesnt work without this sout
                cuenta.setNumCuenta(Integer.parseInt(vista.txtNumCuenta.getText()));
                cuenta.setCliente(
                        new Cliente(
                                vista.txtNombre.getText(),
                                vista.txtDomicilio.getText(),
                                vista.txtFechaNacimiento.getText(),
                                option 
                        )
                );
                cuenta.setNombreBanco(vista.txtNombreBanco.getText());
                cuenta.setFechaApertura(vista.txtFechaApertura.getText());
                cuenta.setPorcentajeRendimiento(Integer.parseInt(vista.txtFechaApertura.getText()));
                cuenta.setSaldo(Float.parseFloat(vista.txtSaldo.getText()));
                }
            catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            
            limpiar();

            // Unlocks mostrar
            vista.btnMostrar.setEnabled(true);

            // Unlocks other options
            vista.txtCantidad.setEnabled(true);
            vista.btnDeposito.setEnabled(true);
            vista.btnRetiro.setEnabled(true);
        }
        
        if(e.getSource() == vista.btnMostrar) {
            vista.txtNumCuenta.setText(Integer.toString(cuenta.getNumCuenta()));
            vista.txtNombre.setText(cuenta.getCliente().getNombre());
            vista.txtDomicilio.setText(cuenta.getCliente().getDomicilio());
            vista.txtFechaNacimiento.setText(cuenta.getCliente().getFechaNacimiento());
            if(cuenta.getCliente().getSexo().equals("Sexo Masculino"))
                vista.radioButtonSexoMasculino.setSelected(true);
            else if(cuenta.getCliente().getSexo().equals("Sexo Femenino"))
                vista.radioButtonSexoFemenino.setSelected(true);
            vista.txtNombreBanco.setText(cuenta.getNombreBanco());
            vista.txtFechaApertura.setText(cuenta.getFechaApertura());
            vista.txtPorcentajeRendimiento.setText(Integer.toString(cuenta.getPorcentajeRendimiento()));
            vista.txtSaldo.setText(Float.toString(cuenta.getSaldo()));
            
            vista.txtNuevoSaldo.setText(Float.toString(cuenta.getSaldo()));
            vista.txtReditos.setText(Float.toString(cuenta.calcularReditos()));
        }
        
        if(e.getSource() == vista.btnDeposito) {
            // Add error to numbers equal or smaller than 0
            try {
                if(Float.parseFloat(vista.txtCantidad.getText()) <= 0)
                    throw new IllegalArgumentException("No se aceptan negativos ni nulos...");
            }
            catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            cuenta.depositar(Float.parseFloat(vista.txtCantidad.getText()));
            vista.txtNuevoSaldo.setText(Float.toString(cuenta.getSaldo()));
            vista.txtReditos.setText(Float.toString(cuenta.calcularReditos()));
            JOptionPane.showMessageDialog(vista, "¡Depositado exitosamente!");
        }
        
        if(e.getSource() == vista.btnRetiro) {
            // Add error to numbers equal or smaller than 0
            try {
                if(Float.parseFloat(vista.txtCantidad.getText()) <= 0)
                    throw new IllegalArgumentException("No se aceptan negativos ni nulos...");
            }
            catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            if(!cuenta.retirar(Float.parseFloat(vista.txtCantidad.getText()))) {
                int option = JOptionPane.showConfirmDialog(vista, "No tienes fondos suficientes, ¿quieres intentar de nuevo?", "Decide", JOptionPane.YES_NO_OPTION);
                if(option == JOptionPane.YES_OPTION)
                    JOptionPane.showMessageDialog(vista, "Cambie la cantidad, por favor...");
                else
                    JOptionPane.showMessageDialog(vista, ">:(");
            }
            else {
                vista.txtNuevoSaldo.setText(Float.toString(cuenta.getSaldo()));
                vista.txtReditos.setText(Float.toString(cuenta.calcularReditos()));
                JOptionPane.showMessageDialog(vista, "¡Dinero retirado correctamente!");
            }
        }
        
        
        
        
    }
    
    public void iniciarVista() {
        vista.setTitle("Banco");
        vista.setSize(650, 600);
        vista.setVisible(true);
    }
    
    public void limpiar() {
        // Limpiar sexo -->
        vista.radioButtonSexoMasculino.setSelected(false);
        vista.radioButtonSexoFemenino.setSelected(false);
        vista.txtNumCuenta.setText("");
        vista.txtNombre.setText("");
        vista.txtDomicilio.setText("");
        vista.txtFechaNacimiento.setText("");
        vista.txtNombreBanco.setText("");
        vista.txtFechaApertura.setText("");
        vista.txtPorcentajeRendimiento.setText("");
        vista.txtSaldo.setText("");
        vista.txtCantidad.setText("");
        vista.txtNuevoSaldo.setText("");
        vista.txtReditos.setText("");
    }
}
