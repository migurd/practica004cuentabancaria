
package Modelo;

/**
 *
 * @author quier
 */
public class Cliente {
    private String nombre;
    private String fechaNacimiento;
    private String domicilio;
    private String sexo;

    public Cliente() {
        this.nombre = "";
        this.fechaNacimiento = "";
        this.domicilio = "";
        this.sexo = "";
    }
    
    public Cliente(Cliente otro) {
        this.nombre = otro.nombre;
        this.fechaNacimiento = otro.fechaNacimiento;
        this.domicilio = otro.domicilio;
        this.sexo = otro.sexo;
    }
    
    public Cliente(String nombre, String fechaNacimiento, String domicilio, String sexo) {
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.domicilio = domicilio;
        this.sexo = sexo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    
}
