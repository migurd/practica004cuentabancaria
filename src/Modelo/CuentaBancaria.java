
package Modelo;

/**
 *
 * @author quier
 */
public class CuentaBancaria {
    
    private int numCuenta;
    private Cliente cliente;
    private String fechaApertura;
    private String nombreBanco;
    private int porcentajeRendimiento;
    private float saldo;

    public CuentaBancaria() {
        this.numCuenta = 0;
        this.cliente = new Cliente();
        this.fechaApertura = "";
        this.nombreBanco = "";
        this.porcentajeRendimiento = 0;
        this.saldo = 0;
    }
    
    public CuentaBancaria(CuentaBancaria otro) {
        this.numCuenta = otro.numCuenta;
        this.cliente = otro.cliente;
        this.fechaApertura = otro.fechaApertura;
        this.nombreBanco = otro.nombreBanco;
        this.porcentajeRendimiento = otro.porcentajeRendimiento;
        this.saldo = otro.saldo;
    }
    
    public CuentaBancaria(int numCuenta, Cliente cliente, String fechaApertura, String nombreBanco, int porcentajeRendimiento, float saldo) {
        this.numCuenta = numCuenta;
        this.cliente = cliente;
        this.fechaApertura = fechaApertura;
        this.nombreBanco = nombreBanco;
        this.porcentajeRendimiento = porcentajeRendimiento;
        this.saldo = saldo;
    }
    
    public int getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(String fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public int getPorcentajeRendimiento() {
        return porcentajeRendimiento;
    }

    public void setPorcentajeRendimiento(int porcentajeRendimiento) {
        this.porcentajeRendimiento = porcentajeRendimiento;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
    
    public void depositar(float deposito) {
        this.saldo += deposito;
    }
    
    public boolean retirar(float retiro) {
        if(retiro > this.saldo)
            return false;
        // Withdrawal successful
        this.saldo -= retiro;
        return true;
    }
    
    // Capital ganado sin contar el primer dinero
    public float calcularReditos() {
        return (float)this.porcentajeRendimiento * (this.saldo / (float)365);
    }
    
}
